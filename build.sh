#!/bin/bash

function pt_error()
{
    echo -e "\033[1;31mERROR: $*\033[0m"
    exit 1
}

function pt_warn()
{
    echo -e "\033[1;31mWARN: $*\033[0m"
}

function pt_info()
{
    echo -e "\033[1;32mINFO: $*\033[0m"
}

function execute_cmd() 
{
    eval $@ || exit $?
}

if [ $# -eq 1 ]; then
	target=${1}
	if [ "x${target}" != "xbootimage" ]; then
		echo -e "\033[1;32mUsage: `basename $0` [bootimage]\033[0m"
		exit 1
	fi
fi

export PATH=/usr/lib/jvm/java-8-openjdk-amd64/bin/:$PATH
source ./build/envsetup.sh
execute_cmd "lunch cheetah_fvd_p1-eng"
execute_cmd "extract-bsp"
execute_cmd "make update-api"
execute_cmd "make -j8 ${target}"
execute_cmd "pack"
