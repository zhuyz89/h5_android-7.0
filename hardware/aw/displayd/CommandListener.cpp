/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <errno.h>
#include <string>

#include <linux/if.h>
#include <cutils/log.h>
#include <sysutils/SocketClient.h>

#include "CommandListener.h"
#include "ResponseCode.h"
#include "commands.h"
#include "debug.h"

/*
 * Convert idx from framework to the local define value.
 *
 * Type define in android.view.display:
 *  - TYPE_UNKNOWN  = 0
 *  - TYPE_BUILT_IN = 1 (primary display)
 *
 * idx used in displayd/hwc:
 *  - primary display = 0
 *  - aux display     = 1
 */
static int toDisplayIdx(int framework_display_type) {
	int idx = 0;
	switch (framework_display_type) {
	case 0:
	case 1:
		idx = 0;
		break;
	default:
		idx = 1;
		break;
    }
	return idx;
}

DisplaydCommand::DisplaydCommand(const char *cmd)
	:FrameworkCommand(cmd) {
}

int
DisplaydCommand::toCommandCode(char *name) {
	auto iter = mCommandMaps.find(name);
	if (iter == mCommandMaps.end())
		return -1;
	return iter->second;
}

#define CHECK_ARGC(__client, __argc, __limit)               \
if (__argc < __limit) {                                     \
	__client->sendMsg(ResponseCode::CommandSyntaxError,     \
								"Missing argument", false); \
	return 0;                                               \
}

#define UNKNOW_COMMAND(__client)                            \
__client->sendMsg(ResponseCode::CommandSyntaxError,         \
				 				"Unkown command", false)

CommandListener::CommandListener(DisplayManager *dm)
	:FrameworkListener("displayd", true) {
	registerCmd(new InterfaceCmd(dm));
	registerCmd(new OverscanCmd(dm));
	registerCmd(new UtilsCmd(dm));
	mDisplayManager = dm;
}

CommandListener::InterfaceCmd::InterfaceCmd(DisplayManager *dm)
	:DisplaydCommand("interface"), mManager(*dm) {

	mCommandMaps.insert(commandMaps_t::value_type("GetFormat", CMD_IFACE_GET_FORMAT));
	mCommandMaps.insert(commandMaps_t::value_type("SetFormat", CMD_IFACE_SET_FORMAT));
	mCommandMaps.insert(commandMaps_t::value_type("GetType",   CMD_IFACE_GET_TYPE));
	mCommandMaps.insert(commandMaps_t::value_type("GetMode",   CMD_IFACE_GET_MODE));
	mCommandMaps.insert(commandMaps_t::value_type("SetMode",   CMD_IFACE_SET_MODE));
	mCommandMaps.insert(commandMaps_t::value_type("CheckMode", CMD_IFACE_CHECK_MODE));
	mCommandMaps.insert(commandMaps_t::value_type("ListMode",  CMD_IFACE_LIST_MODE));
}

int
CommandListener
::InterfaceCmd::runCommand(SocketClient *client, int argc, char **argv) {
	CHECK_ARGC(client, argc, 3);

	char response[1024];
	int command = toCommandCode(argv[1]);
	int display = toDisplayIdx(atoi(argv[2]));
	int format, mode, type;
	std::vector<int> supportModes;
	int error = 0;

	dumpArguments("Framework --> displayd", argc, argv);
	switch (command) {
	case CMD_IFACE_GET_FORMAT:
		format = mManager.getOutputFormat(display);
		sprintf(response, "%d", format);
		break;
	case CMD_IFACE_SET_FORMAT:
		CHECK_ARGC(client, argc, 4);
		format = atoi(argv[3]);
		error = mManager.setOutputFormat(display, format);
		sprintf(response, "%s", !error ? "Set format completed" : "unknow error");
		break;
	case CMD_IFACE_GET_TYPE:
		type = mManager.getOutputType(display);
		sprintf(response, "%d", type);
		break;
	case CMD_IFACE_GET_MODE:
		mode = mManager.getOutputMode(display);
		sprintf(response, "%d", mode);
		break;
	case CMD_IFACE_SET_MODE:
		CHECK_ARGC(client, argc, 4);
		mode = atoi(argv[3]);
		error = mManager.setOutputMode(display, mode);
		sprintf(response, "%s", !error ? "Set mode completed" : "unknow error");
		break;
	case CMD_IFACE_CHECK_MODE:
		CHECK_ARGC(client, argc, 4);
		mode = atoi(argv[3]);
		sprintf(response, "%s", mManager.isSupportMode(display, mode) == 1 ? "true" : "false");
		break;
	case CMD_IFACE_LIST_MODE:
		mManager.listSupportModes(display, &supportModes);
		for (auto& item : supportModes) {
			sprintf(response, "%d", item);
			client->sendMsg(ResponseCode::ModeListResult, response, false);
		}
		sprintf(response, "%s", "Interface list completed");
		break;
	default:
		sprintf(response, "unknow commands");
		error = -1;
		break;
	}
	client->sendMsg(!error ? ResponseCode::CommandOkay : ResponseCode::OperationFailed, response, false);
	return 0;
}

CommandListener::OverscanCmd::OverscanCmd(DisplayManager *dm)
	:DisplaydCommand("overscan"), mManager(*dm) {

	mCommandMaps.insert(commandMaps_t::value_type("GetMargin", CMD_OVERSCAN_GET_MARGIN));
	mCommandMaps.insert(commandMaps_t::value_type("SetMargin", CMD_OVERSCAN_SET_MARGIN));
	mCommandMaps.insert(commandMaps_t::value_type("GetOffset", CMD_OVERSCAN_GET_OFFSET));
	mCommandMaps.insert(commandMaps_t::value_type("SetOffset", CMD_OVERSCAN_SET_OFFSET));
}

int
CommandListener
::OverscanCmd::runCommand(SocketClient *client, int argc, char **argv) {
	CHECK_ARGC(client, argc, 3);

	int command = toCommandCode(argv[1]);
	int display = toDisplayIdx(atoi(argv[2]));
	int xvalue, yvalue;
	char response[1024];
	int error = 0;

	dumpArguments("Framework --> displayd", argc, argv);
	switch (command) {
	case CMD_OVERSCAN_GET_MARGIN:
		xvalue = yvalue = 0;
		mManager.getDisplayMargin(display, &xvalue, &yvalue);
		sprintf(response, "%d %d", xvalue, yvalue);
		break;
	case CMD_OVERSCAN_SET_MARGIN:
		CHECK_ARGC(client, argc, 5);
		xvalue = atoi(argv[3]);
		yvalue = atoi(argv[4]);
		error = mManager.setDisplayMargin(display, xvalue, yvalue);
		sprintf(response, "%s", !error ? "Set margin completed" : "unknow error");
		break;
	case CMD_OVERSCAN_GET_OFFSET:
		xvalue = yvalue = 0;
		mManager.getDisplayOffset(display, &xvalue, &yvalue);
		sprintf(response, "%d %d", xvalue, yvalue);
		break;
	case CMD_OVERSCAN_SET_OFFSET:
		CHECK_ARGC(client, argc, 5);
		xvalue = atoi(argv[3]);
		yvalue = atoi(argv[4]);
		error = mManager.setDisplayOffset(display, xvalue, yvalue);
		sprintf(response, "%s", !error ? "Set offset completed" : "unknow error");
		break;
	default:
		sprintf(response, "unknow commands");
		error = -1;
		break;
	}
	client->sendMsg(!error ? ResponseCode::CommandOkay : ResponseCode::OperationFailed, response, false);
	return 0;
}

CommandListener::UtilsCmd::UtilsCmd(DisplayManager *dm)
	:DisplaydCommand("utils"), mManager(*dm) {

	mCommandMaps.insert(commandMaps_t::value_type("Set3DLayerMode", CMD_UTILS_SET_3D_LAYER_MODE));
	mCommandMaps.insert(commandMaps_t::value_type("Set3DLayerOffset", CMD_UTILS_SET_3D_LAYER_OFFSET));
	mCommandMaps.insert(commandMaps_t::value_type("GetSupport3DMode", CMD_UTILS_GET_SUPPORT_3D_MODE));
}

int
CommandListener
::UtilsCmd::runCommand(SocketClient *client, int argc, char **argv) {
	CHECK_ARGC(client, argc, 3);

	int command = toCommandCode(argv[1]);
	int display = toDisplayIdx(atoi(argv[2]));
	int offset;
	int mode;
	int support;
	char response[1024];
	int error = 0;

	dumpArguments("Framework --> displayd", argc, argv);
	switch (command) {
	case CMD_UTILS_SET_3D_LAYER_MODE:
		CHECK_ARGC(client, argc, 4);
		mode = atoi(argv[3]);
		error = mManager.set3DLayerMode(display, mode);
		sprintf(response, "%s", !error ? "Set 3D layer mode completed" : "unknow error");
		break;
	case CMD_UTILS_SET_3D_LAYER_OFFSET:
		CHECK_ARGC(client, argc, 4);
		offset = atoi(argv[3]);
		error = 0;
		sprintf(response, "%s", !error ? "Set 3D layer offset completed" : "unknow error");
		break;
	case CMD_UTILS_GET_SUPPORT_3D_MODE:
		support = mManager.isSupport3DMode(display, -1);
		sprintf(response, "%d", support);
		break;
	default:
		sprintf(response, "unknow commands");
		error = -1;
		break;
	}
	client->sendMsg(!error ? ResponseCode::CommandOkay : ResponseCode::OperationFailed, response, false);
	return 0;
}

#if 0
int
CommandListener
::InterfaceCmd::runCommand(SocketClient *client, int argc, char **argv) {
	CHECK_ARGC(client, argc, 3);

	int command = toCommandCode(argv[1]);
	int display = atoi(argv[2]);

	switch (command) {
	case CMD_LIST_INTERFACE: {
		std::vector<int> supportInterface;
		mManager.listInterface(display, &supportInterface);
		for (auto& interface : supportInterface) {
			client->sendMsg(ResponseCode::InterfaceListResult, interfaceToString(interface), false);
		}
		client->sendMsg(ResponseCode::CommandOkay, "Interface list completed", false);
		break; }
	case CMD_GET_CURRENT_INTERFACE: {
		int interface;
		mManager.getCurrentInterface(display, &interface);
		client->sendMsg(ResponseCode::CommandOkay, interfaceToString(interface), false);
		break; }
	case CMD_SET_CURRENT_INTERFACE: {
		int interface = stringToInterface(argv[3]);
		int enable = strcmp(argv[4], "true") == 0 ? 1 : 0;
		int result = mManager.setCurrentInterface(display, interface, enable);
		int response = result ? ResponseCode::OperationFailed : ResponseCode::CommandOkay;
		const char *message = result ? "Interface set failed" : "Interface set completed";
		client->sendMsg(response, message, false);
		break; }
	case CMD_SWITCH_INTERFACE:
		ALOGD("request: display %d switch to next interface", display);
		client->sendMsg(ResponseCode::CommandOkay, "Switch interface completed", false);
		break;
	default:
		UNKNOW_COMMAND(client);
		break;
	}
	return 0;
}

CommandListener::ModeCmd::ModeCmd(DisplayManager *dm)
	:DisplaydCommand("mode"), mManager(*dm) {

	mCommandMaps.insert(commandMaps_t::value_type("list",       CMD_LIST_MODE));
	mCommandMaps.insert(commandMaps_t::value_type("get",        CMD_GET_CURRENT_MODE));
	mCommandMaps.insert(commandMaps_t::value_type("set",        CMD_SET_CURRENT_MODE));
	mCommandMaps.insert(commandMaps_t::value_type("get3dmodes", CMD_GET_3D_MODES));
	mCommandMaps.insert(commandMaps_t::value_type("get3dmode",  CMD_GET_3D_MODE));
	mCommandMaps.insert(commandMaps_t::value_type("set3dmode",  CMD_SET_3D_MODE));
}

int CommandListener::ModeCmd::runCommand(SocketClient *client, int argc, char **argv) {
	CHECK_ARGC(client, argc, 4);

	int command = toCommandCode(argv[1]);
	int display = atoi(argv[2]);
	int interface = stringToInterface(argv[3]);

	switch (command) {
	case CMD_LIST_MODE: {
		std::vector<int> supportModes;
		mManager.listMode(display, interface, &supportModes);
		for (auto& mode : supportModes) {
			client->sendMsg(ResponseCode::ModeListResult, modeToString(mode), false);
		}
		client->sendMsg(ResponseCode::CommandOkay, "Mode list completed", false);
		break; }
	case CMD_GET_CURRENT_MODE: {
		int mode;
		if (mManager.getCurrentMode(display, interface, &mode))
			client->sendMsg(ResponseCode::CommandParameterError, "Missing iface", false);

		client->sendMsg(ResponseCode::CommandOkay, modeToString(mode), false);
		break; }
	case CMD_SET_CURRENT_MODE: {
		int mode = stringToMode(argv[4]);
		mManager.setCurrentMode(display, interface, mode);
		client->sendMsg(ResponseCode::CommandOkay, "Mode set completed", false);
		break; }
	case CMD_GET_3D_MODES:
	case CMD_GET_3D_MODE:
		ALOGD("request: display %d get 3D mode", display);
		client->sendMsg(ResponseCode::CommandOkay, "completed", false);
		break;
	case CMD_SET_3D_MODE:
		ALOGD("request: display %d set 3D mode", display);
		client->sendMsg(ResponseCode::CommandOkay, "completed", false);
		break;
	default:
		UNKNOW_COMMAND(client);
		break;
	}
	return 0;
}
#endif

