/*$_FOR_ROCKCHIP_RBOX_$*/
//$_rbox_$_modify_$_chenzhi_20120309: add for pppoe

#define LOG_TAG "android_pppoe_PppoeNative.cpp"

#include "jni.h"
#include <utils/misc.h>
#include <android_runtime/AndroidRuntime.h>
#include <utils/Log.h>
#include <cutils/properties.h>

#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define LOGD ALOGD
#define LOGE ALOGE
#define LOGV ALOGV

namespace android {

static jint startPppoeNative(JNIEnv* env, jobject clazz,jstring iface)
{
        int attempt;
        int err = 0;
        char* prop = (char *)malloc(PROPERTY_VALUE_MAX);
        LOGD("%s",__FUNCTION__);
        property_set("net.eth0-pppoe.status","starting");

        if (property_set("ctl.start", "pppoe_eth0") < 0) {
            LOGE("Failed to start pppoe_eth0 service!");
            err = -1;
            goto end;
        }
        for (attempt = 3000; attempt > 0;  attempt--) {
            property_get("net.eth0-pppoe.status", prop, "");
            if (!strcmp(prop, "started")) {
                break;
            }
            if (!strcmp(prop, "stopped")) { //在pppoe进程退出时，我们会设置该状态。
                attempt = 0;
                err = -1;
                break;
            }
            usleep(100000);  // 100 ms retry delay
        }
        if (attempt == 0) {
            LOGE("%s: Timeout waiting for pppoe-start", __FUNCTION__);
            err = -1;
            goto end;
        }
        err = 0;
end:
    free(prop);
    return err;
/* pid_t pid ;
 int UINT;
 char *Iface = (char *)env->GetStringUTFChars(iface, NULL);
 
    if ((pid = fork()) < 0) {
        ALOGE("fork failed (%s)", strerror(errno));
        return -1;
    }

    if (!pid) {

        // TODO: Deal with pppd bailing out after 99999 seconds of being started
        // but not getting a connection
        if(strcmp(Iface,"eth0")) {
           UINT = 0;
        }
        else {
           UINT = 1;
        }
         env->ReleaseStringUTFChars(iface,Iface);
         umask(0);
        if (execl("/system/bin/pppoe", "/system/bin/pppoe", "plugin-pppoe", "nic-eth0", "unit",
                  "0", "user", "39300068225@139.gd", "linkinterface", Iface, "call", "pppoe-options", (char *) NULL)) {
            ALOGE("execl failed (%s)", strerror(errno));
            return 0;
        }
       
        ALOGE("execl successful,start pppoe!");
  }
       
       return 1;*/
}

static jint stopPppoeNative(JNIEnv* env, jobject clazz,jstring iface)
{
    int attempt;
    int err = 0;
    char* prop = (char *)malloc(PROPERTY_VALUE_MAX);
    LOGD("%s", __FUNCTION__);
    if (property_get("net.eth0-pppoe.status",prop,"")) {
         if (!strcmp(prop, "stopped") || !strcmp(prop,"")) {
            err = 0;
            goto end;
         }
    }
     property_set("net.eth0-pppoe.status","stopping");
    if (property_set("ctl.start", "stop_pppoe_eth0") < 0) {
        LOGE("Failed to stop pppoe_eth0");
        err = -1;
        goto end;
    }
    
    for (attempt = 100; attempt > 0;  attempt--) {
        property_get("init.svc.pppoe_eth0", prop, ""); //直接判断pppoe_eth0的状态是否是stopped.
        if (!strcmp(prop, "stopped") || !strcmp(prop,"")) {
            property_set("net.eth0-pppoe.status","stopped");
            err = 0;
            goto end;
        }
        usleep(100000);  // 100 ms retry delay
    }
    err = -1;
end:
    free(prop);
    return err;
#if 0
  pid_t pid ;
  char *Iface = (char *)env->GetStringUTFChars(iface, NULL);

    if ((pid = fork()) < 0) {
        ALOGE("fork failed (%s)", strerror(errno));
        return -1;
    }

    if (!pid) {
        env->ReleaseStringUTFChars(iface,Iface);
        if (execl("/system/bin/pppoe-disconnect", "/system/bin/pppoe-disconnect", Iface , (char *) NULL)) {
            ALOGE("execl failed (%s)", strerror(errno));
            return 0;
        }
        ALOGE("execl successful,start pppoe!");
        env->ReleaseStringUTFChars(iface,Iface);

  }

       return 1;
#endif

}
    
/*---------------------------------------------------------------------------*/

/*
 * JNI registration.
 */
static JNINativeMethod gPppoeMethods[] = {
    /* name,                    method descriptor,                              funcPtr */
    { "startPppoeNative",       "(Ljava/lang/String;)I",                                          (void *)startPppoeNative },
    { "stopPppoeNative",        "(Ljava/lang/String;)I",                                          (void *)stopPppoeNative },
};

int register_android_pppoe_PppoeNative(JNIEnv* env)
{
    return AndroidRuntime::registerNativeMethods(env, "com/android/server/pppoe/PppoeNetworkFactory", gPppoeMethods, NELEM(gPppoeMethods) );
}

/* User to register native functions */
extern "C"
jint Java_com_android_server_pppoe_PppoeNetworkFactory_registerNatives(JNIEnv* env, jclass clazz) {
   return AndroidRuntime::registerNativeMethods(env, "com/android/server/pppoe/PppoeNetworkFactory", gPppoeMethods, NELEM(gPppoeMethods) );
}
	
}


