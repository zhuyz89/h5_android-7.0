package com.softwinner.dragonbox.testcase;

import java.io.IOException;
import java.io.File;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.TextView;
import android.media.AudioManager;
import android.media.AudioManagerEx;
import com.softwinner.dragonbox.utils.AudioChannelUtil;


import com.softwinner.dragonbox.R;
import com.softwinner.dragonbox.manager.MicRecoredManager;

public class CaseMicRecored extends IBaseCase {
	private static final int WHAT_COUNT_DOWN = 1;
	private static final int WHAT_COUNT_DOWN_FINISH = 2;

	private int mRecoredTime;
	MicRecoredManager mMicRecoredManager;
	TextView mMinMicRecoredStatus;
	TextView mMaxMicRecoredStatus;
	private AudioManager mAM;
	private int lastStreamVolume;

	Handler mHandler = new Handler(Looper.myLooper()) {
		@Override
		public void handleMessage(Message message) {
			switch (message.what) {
			case WHAT_COUNT_DOWN:
				mMaxMicRecoredStatus.setText(mContext.getString(R.string.case_micrecored_info, message.arg1));
				break;
			case WHAT_COUNT_DOWN_FINISH:
				setDialogPositiveButtonEnable(true);
				mMaxMicRecoredStatus.setText(R.string.case_micrecored_play_info);
				mMicRecoredManager.stopRecorder();
				try {
					mMicRecoredManager.playRecorder();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;

			}
		}
	};

	public CaseMicRecored(Context context) {
		super(context, R.string.case_micrecored_name, R.layout.case_micrecored_max,
				R.layout.case_micrecored_min, TYPE_MODE_MANUAL);
		mMicRecoredManager = new MicRecoredManager(context);
		mMinMicRecoredStatus = (TextView) mMinView
				.findViewById(R.id.case_micrecored_status);
		mMaxMicRecoredStatus = (TextView) mMaxView
				.findViewById(R.id.case_micrecored_max_status);
		mAM= (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	}

	public CaseMicRecored(Context context, XmlPullParser xmlParser) {
		this(context);
		String time = xmlParser.getAttributeValue(null, "recoredtime");
		try{
			mRecoredTime = Integer.parseInt(time);
		} catch (Exception e) {
			e.printStackTrace();
			mRecoredTime = 5;
		}
	}

	@Override
	public void onStartCase() {
		lastStreamVolume = mAM.getStreamVolume(AudioManager.STREAM_MUSIC);
		int maxVolume = mAM.getStreamMaxVolume(AudioManager.STREAM_MUSIC);//set the max Volume before testing
        mAM.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
        AudioChannelUtil.setOuputChannels(mContext,true,AudioManagerEx.AUDIO_NAME_CODEC,AudioManagerEx.AUDIO_NAME_HDMI);
		setDialogPositiveButtonEnable(false);
		try {
			mMicRecoredManager.startRecorder();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Message msg = mHandler.obtainMessage(WHAT_COUNT_DOWN_FINISH);
		//mHandler.handleMessage(msg);
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < mRecoredTime; i++) {
					final int time = mRecoredTime - i;
					//Message msg = mHandler.obtainMessage(WHAT_COUNT_DOWN);
					Message msg = new Message();
					msg.what = WHAT_COUNT_DOWN;
					msg.arg1 = time;
					mHandler.sendMessage(msg);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				Message msg = mHandler.obtainMessage(WHAT_COUNT_DOWN_FINISH);
				mHandler.sendMessage(msg);
			}
		}).start();

	}

	@Override
	public void onStopCase() {
		mMicRecoredManager.release();
		mMinMicRecoredStatus
				.setText(getCaseResult() ? R.string.case_micrecored_status_success_text
						: R.string.case_micrecored_status_fail_text);
		mAM.setStreamVolume(AudioManager.STREAM_MUSIC, lastStreamVolume , 0);//restore StreamVolume after testing
        AudioChannelUtil.setOuputChannels(mContext,false,AudioManagerEx.AUDIO_NAME_CODEC,AudioManagerEx.AUDIO_NAME_HDMI);
		File file = new File(mMicRecoredManager.mFilePath); 
		if(file.exists()){
			file.delete();
		}	
	}

	@Override
	public void reset() {
		super.reset();
		mMinMicRecoredStatus.setText(R.string.case_micrecored_status_text);

	}

}
