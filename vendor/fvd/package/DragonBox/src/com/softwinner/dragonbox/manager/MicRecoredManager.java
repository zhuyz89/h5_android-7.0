package com.softwinner.dragonbox.manager;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.widget.Toast;
import com.softwinner.dragonbox.R;
public class MicRecoredManager {
	Context mContext;
	
	private MediaPlayer mMediaplayer = null;  
    private MediaRecorder mRecorder = null; 
    
    private static final String OUTPUT_FILE = "audiorecordtest.3gp";  
    public String mFilePath;
	public MicRecoredManager(Context context) {
		mContext = context;
		mFilePath = mContext.getCacheDir().getPath() + File.separator + OUTPUT_FILE;
	}

	public void startRecorder(){
		if (mRecorder == null) {
			mRecorder = new MediaRecorder();
		}
        //mRecorder.setAudioSource(MediaRecorder.AudioSource.CMCC_KARAOK_MIC);
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mRecorder.setOutputFile(mFilePath);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        try {
            mRecorder.prepare();
        } catch (IOException e) {
        	e.printStackTrace();
        }
        mRecorder.start();  
	}

	public void stopRecorder(){
		if (mRecorder != null) {
            try{
			    mRecorder.stop();	
            }catch(Exception e){
                Toast.makeText(mContext,R.string.get_audio_device_fail,0).show();
                mRecorder.release();
                mRecorder = null;
            }
		}
	}
	
	public void playRecorder() throws IOException{
		if (mMediaplayer == null) {
			mMediaplayer = new MediaPlayer();			
		}
		mMediaplayer.reset();
		mMediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		 mMediaplayer.setDataSource(mFilePath);
		//mMediaplayer.setOnErrorListener(this);
		//mMediaplayer.setOnBufferingUpdateListener(this);
		mMediaplayer.setOnPreparedListener(new OnPreparedListener(){
			public void onPrepared (MediaPlayer mp){
				mMediaplayer.start();
			}
		});
		//mMediaplayer.setOnInfoListener(this);
		//mMediaplayer.setOnCompletionListener(this);
		mMediaplayer.prepareAsync();
	}
	
	public void release(){
		if (mMediaplayer != null && mMediaplayer.isPlaying()) {
			mMediaplayer.stop();
			mMediaplayer.release();
			mMediaplayer = null;
		}
		if (mRecorder != null) {
			mRecorder.release();
			mRecorder = null;
		}
	}
	
	
}
