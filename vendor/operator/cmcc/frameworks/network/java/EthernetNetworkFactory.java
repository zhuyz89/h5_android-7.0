/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpResults;
import android.net.DhcpInfo;
import android.net.NetworkUtils;
import android.net.ethernet.EthernetManager;
import android.net.pppoe.PppoeManager;
import android.net.IEthernetServiceListener;
import android.net.InterfaceConfiguration;
import android.net.IpConfiguration;
import android.net.LinkAddress;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.net.LinkProperties;
import android.net.NetworkAgent;
import android.net.NetworkCapabilities;
import android.net.NetworkFactory;
import android.net.NetworkInfo;
import android.net.RouteInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.StaticIpConfiguration;
import android.net.ip.IpManager;
import android.net.RouteInfo;
import android.net.ip.IpManager.ProvisioningConfiguration;
import android.net.ip.IpManager.WaitForProvisioningCallback;
import android.os.Handler;
import android.os.IBinder;
import android.os.INetworkManagementService;
import android.os.Looper;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import android.content.Intent;
import android.os.UserHandle;
import android.content.ContentResolver;
import android.provider.Settings;
import android.net.ConnectivityManager;
import com.android.internal.util.IndentingPrintWriter;
import com.android.server.net.BaseNetworkObserver;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.List;
import java.util.ArrayList;

/**
 * Manages connectivity for an Ethernet interface.
 *
 * Ethernet Interfaces may be present at boot time or appear after boot (e.g.,
 * for Ethernet adapters connected over USB). This class currently supports
 * only one interface. When an interface appears on the system (or is present
 * at boot time) this class will start tracking it and bring it up, and will
 * attempt to connect when requested. Any other interfaces that subsequently
 * appear will be ignored until the tracked interface disappears. Only
 * interfaces whose names match the <code>config_ethernet_iface_regex</code>
 * regular expression are tracked.
 *
 * This class reports a static network score of 70 when it is tracking an
 * interface and that interface's link is up, and a score of 0 otherwise.
 *
 * @hide
 */
class EthernetNetworkFactoryEx {
    private static final String NETWORK_TYPE = "Ethernet";
    private static final String TAG = "EthernetNetworkFactoryEx";
    private static final int NETWORK_SCORE = 70;
    private static final boolean DBG = true;

    /** Tracks interface changes. Called from NetworkManagementService. */
    private InterfaceObserver mInterfaceObserver;

    /** For static IP configuration */
    private  EthernetManager mEthernetManager;

    /** To set link state and configure IP addresses. */
    private INetworkManagementService mNMService;

    /* To communicate with ConnectivityManager */
    private NetworkCapabilities mNetworkCapabilities;
    private NetworkAgent mNetworkAgent;
    private LocalNetworkFactory mFactory;
    private ConnectivityManager mConnectivityManager;
    /** Product-dependent regular expression of interface names we track. */
    private static String mIfaceMatch = "";

    /** Data members. All accesses to these must be synchronized(this). */
    private static String mIface = "";  //mIface which own network.
    private static String mPhyIface = "eth\\d";
    private String mHwAddr;
    private static boolean mLinkUp;
    private int mPppoeState;
    private NetworkInfo mNetworkInfo;
    private LinkProperties mLinkProperties;
    private IpManager mIpManager;
    private Thread mIpProvisioningThread;
    private DhcpResults mDhcpResults;
    private Context mContext;
    static {
         System.loadLibrary("pppoe-cmcc-jni");
         registerNatives();
    }
    EthernetNetworkFactoryEx() {
        mNetworkInfo = new NetworkInfo(ConnectivityManager.TYPE_ETHERNET, 0, NETWORK_TYPE, "");
        mLinkProperties = new LinkProperties();
        initNetworkCapabilities();
        mDhcpResults = new DhcpResults();
    }

    private class LocalNetworkFactory extends NetworkFactory {
        LocalNetworkFactory(String name, Context context, Looper looper) {
            super(looper, context, name, new NetworkCapabilities());
        }

        protected void startNetwork() {
            onRequestNetwork();
        }
        protected void stopNetwork() {
        }
    }

    private void stopIpManagerLocked() {
        if (mIpManager != null) {
            mIpManager.shutdown();
            mIpManager = null;
        }
    }

    private void stopIpProvisioningThreadLocked() {
        stopIpManagerLocked();

        if (mIpProvisioningThread != null) {
            mIpProvisioningThread.interrupt();
            mIpProvisioningThread = null;
        }
    }

    /**
     * Updates interface state variables.
     * Called on link state changes or on startup.
     */
    private void updateInterfaceState(String iface, boolean up) {
      //  if (!mIface.equals(iface)) {
        if (!iface.matches(mPhyIface)) {
            return;
        }
        Log.d(TAG, "updateInterface: " + iface + " link " + (up ? "up" : "down"));

        synchronized(this) {
            mLinkUp = up;
            mNetworkInfo.setIsAvailable(up);
            if (!up) {
                // Tell the agent we're disconnected. It will call disconnect().
                mNetworkInfo.setDetailedState(DetailedState.DISCONNECTED, null, mHwAddr); //this is enough for dhcp/static to refresh cmcc setting.
                stopIpProvisioningThreadLocked();
                mDhcpResults.clear();
                if (mEthernetManager == null) {
                    mEthernetManager = (EthernetManager)mContext.getSystemService(Context.ETHERNET_SERVICE);
                }
                if ((mEthernetManager.getEthernetMode() == EthernetManager.ETHERNET_CONNECT_MODE_PPPOE) && iface.equals("eth0")) {
                    if (!("stopped").equals(SystemProperties.get("net."+iface+"-pppoe.status"))) {
                       sendPppoeStateBroadcast(PppoeManager.EVENT_DISCONNECT_SUCCESSED); //clear out pppoe connection info when link out.
                       mPppoeState = PppoeManager.PPPOE_STATE_DISCONNECT;
                       stopPppoeNative(iface);
                    }
                }
            }
            else {
                mNetworkInfo.setDetailedState(DetailedState.CONNECTED, null, mHwAddr);
            }
            updateAgent();
            // set our score lower than any network could go
            // so we get dropped.  TODO - just unregister the factory
            // when link goes down.
            mFactory.setScoreFilter(up ? NETWORK_SCORE : -1);
        }
    }

    private class InterfaceObserver extends BaseNetworkObserver {
        @Override
        public void interfaceLinkStateChanged(String iface, boolean up) {
            updateInterfaceState(iface, up);
        }

        @Override
        public void interfaceAdded(String iface) {
            maybeTrackInterface(iface);
        }

        @Override
        public void interfaceRemoved(String iface) {
            stopTrackingInterface(iface);
        }
    }

    private void setInterfaceUp(String iface) {
        // Bring up the interface so we get link status indications.
        try {
            mNMService.setInterfaceUp(iface);
            String hwAddr = null;
            InterfaceConfiguration config = mNMService.getInterfaceConfig(iface);

            if (config == null) {
                Log.e(TAG, "Null iterface config for " + iface + ". Bailing out.");
                return;
            }

            synchronized (this) {
                if (!isTrackingInterface()) {
                    setInterfaceInfoLocked(iface, config.getHardwareAddress());
                    mNetworkInfo.setIsAvailable(true);
                    mNetworkInfo.setExtraInfo(mHwAddr);
                } else {
                    Log.e(TAG, "Interface unexpectedly changed from " + iface + " to " + mIface);
                    mNMService.setInterfaceDown(iface);
                }
            }
        } catch (RemoteException e) {
            Log.e(TAG, "Error upping interface " + mIface + ": " + e);
        }
    }

    private boolean maybeTrackInterface(String iface) {
        // If we don't already have an interface, and if this interface matches
        // our regex, start tracking it.
        if (!iface.matches(mIfaceMatch) || isTrackingInterface())
            return false;

        Log.d(TAG, "Started tracking interface " + iface);
        setInterfaceUp(iface);
        return true;
    }

    private void stopTrackingInterface(String iface) {
        if (!iface.equals(mIface))
            return;

        Log.d(TAG, "Stopped tracking interface " + iface);
        // TODO: Unify this codepath with stop().
        synchronized (this) {
            stopIpProvisioningThreadLocked();
            setInterfaceInfoLocked("", null);
            mNetworkInfo.setExtraInfo(null);
            mLinkUp = false;
            mNetworkInfo.setDetailedState(DetailedState.DISCONNECTED, null, mHwAddr);
            updateAgent();
            mNetworkAgent = null;
            mNetworkInfo = new NetworkInfo(ConnectivityManager.TYPE_ETHERNET, 0, NETWORK_TYPE, "");
            mLinkProperties = new LinkProperties();
        }
    }

    private boolean setStaticIpAddress(StaticIpConfiguration staticConfig) {
        if (staticConfig.ipAddress != null &&
                staticConfig.gateway != null &&
                staticConfig.dnsServers.size() > 0) {
            try {
                Log.i(TAG, "Applying static IPv4 configuration to " + mIface + ": " + staticConfig);
                InterfaceConfiguration config = mNMService.getInterfaceConfig(mIface);
                config.setLinkAddress(staticConfig.ipAddress);
                mNMService.setInterfaceConfig(mIface, config);
                return true;
            } catch(RemoteException|IllegalStateException e) {
               Log.e(TAG, "Setting static IP address failed: " + e.getMessage());
            }
        } else {
            Log.e(TAG, "Invalid static IP configuration.");
        }
        return false;
    }

    public void updateAgent() {
        synchronized (EthernetNetworkFactoryEx.this) {
            if (mNetworkAgent == null) return;
            if (DBG) {
                Log.i(TAG, "Updating mNetworkAgent with: " +
                      mNetworkCapabilities + ", " +
                      mNetworkInfo + ", " +
                      mLinkProperties);
            }
            mNetworkAgent.sendNetworkCapabilities(mNetworkCapabilities);
            mNetworkAgent.sendNetworkInfo(mNetworkInfo);
            mNetworkAgent.sendLinkProperties(mLinkProperties);
            // never set the network score below 0.
            mNetworkAgent.sendNetworkScore(mLinkUp? NETWORK_SCORE : 0);
        }
    }

    /* Called by the NetworkFactory on the handler thread. */
    public void onRequestNetwork() {
        synchronized(EthernetNetworkFactoryEx.this) {
            if (mIpProvisioningThread != null) {
                return;
            }
        }
        if (mEthernetManager == null) {
                mEthernetManager = (EthernetManager)mContext.getSystemService(Context.ETHERNET_SERVICE);
        }
        if (mConnectivityManager == null) {
                mConnectivityManager = (ConnectivityManager)mContext.getSystemService("connectivity");
        }

		String mode = mEthernetManager.getEthernetMode();
        Log.w(TAG,"Ethernet Mode:"+mode);

        final Thread ipProvisioningThread = new Thread(new Runnable() {
            public void run() {
                if (DBG) {
                    Log.d(TAG, String.format("starting ipProvisioningThread(%s): mNetworkInfo=%s",
                            mIface, mNetworkInfo));
                }
                final ContentResolver cr = mContext.getContentResolver();
                //String mode = Settings.Global.getString(cr,Settings.Global.ETHERNET_MODE);

                LinkProperties linkProperties;

               if (mode.equals("manual")) {

                  IpConfiguration config = mEthernetManager.getConfiguration();

                  if (!setStaticIpAddress(config.getStaticIpConfiguration())) {
                        // We've already logged an error.
                        Log.w(TAG,"setStaticIpAddress Error");
						sendEthernetStateBroadcast(EthernetManager.EVENT_STATIC_CONNECT_FAILED);
                        return;
                  }
                    linkProperties = config.getStaticIpConfiguration().toLinkProperties(mIface);
				//	sendEthernetStateBroadcast(EthernetManager.EVENT_STATIC_CONNECT_SUCCESSED);
                    mDhcpResults.ipAddress = config.getStaticIpConfiguration().ipAddress;   //set ipAddress because cmcc apk need it to update UI. 

               } else if(mode.equals("dhcp")){
                    mNetworkInfo.setDetailedState(DetailedState.OBTAINING_IPADDR, null, mHwAddr);
                    WaitForProvisioningCallback ipmCallback = new WaitForProvisioningCallback() {
                        @Override
                        public void onLinkPropertiesChange(LinkProperties newLp) {
                            synchronized(EthernetNetworkFactoryEx.this) {
                                if (mNetworkAgent != null && mNetworkInfo.isConnected()) {
                                    mLinkProperties = newLp;
                                    mNetworkAgent.sendLinkProperties(newLp);
                                }
                            }
                        }
                        public void onNewDhcpResults(DhcpResults dhcpResults) {
                                 mDhcpResults = dhcpResults;
                                 Log.w(TAG,"mDhcpResult = "+mDhcpResults);
                        }
                    };

                    synchronized(EthernetNetworkFactoryEx.this) {
                        stopIpManagerLocked();
                        mIpManager = new IpManager(mContext, mIface, ipmCallback);
/*
                        if (config.getProxySettings() == ProxySettings.STATIC ||
                                config.getProxySettings() == ProxySettings.PAC) {
                            mIpManager.setHttpProxy(config.getHttpProxy());
                        }
*/
                        final String tcpBufferSizes = mContext.getResources().getString(
                                com.android.internal.R.string.config_ethernet_tcp_buffers);
                        if (!TextUtils.isEmpty(tcpBufferSizes)) {
                            mIpManager.setTcpBufferSizes(tcpBufferSizes);
                        }

                        final ProvisioningConfiguration provisioningConfiguration =
                                mIpManager.buildProvisioningConfiguration()
                                        .withProvisioningTimeoutMs(0)
                                        .build();
                        mIpManager.startProvisioning(provisioningConfiguration);
                    }

                    linkProperties = ipmCallback.waitForProvisioning();
                    Log.w(TAG,"linkProperties="+linkProperties);
                    if (linkProperties == null) {
                        Log.e(TAG, "IP provisioning error");
                        // set our score lower than any network could go
                        // so we get dropped.
                        mFactory.setScoreFilter(-1);
                        synchronized(EthernetNetworkFactoryEx.this) {
                            stopIpManagerLocked();
                        }
                        return;
                    }
				//	sendEthernetStateBroadcast(EthernetManager.EVENT_DHCP_CONNECT_SUCCESSED);
                }
                else {
                    mNetworkInfo.setDetailedState(DetailedState.OBTAINING_IPADDR, null, mHwAddr);
                    sendPppoeStateBroadcast(PppoeManager.EVENT_CONNECTING);
                    mPppoeState = PppoeManager.PPPOE_STATE_CONNECTING;
                    DhcpResults dhcpResults = new DhcpResults();
                    if (0 != startPppoeNative("eth0")) {
                        // set our score lower than any network could go
                        // so we get dropped.
                        mFactory.setScoreFilter(-1);
                        mNetworkInfo.setDetailedState(DetailedState.DISCONNECTED, null, mHwAddr);
                        sendPppoeStateBroadcast(PppoeManager.EVENT_CONNECT_FAILED);
                        mPppoeState = PppoeManager.PPPOE_STATE_DISCONNECT;
                        return;
                    }
                    linkProperties = getPppoeLinkProperties("eth0");
                    mPppoeState = PppoeManager.PPPOE_STATE_CONNECT;

                }
                synchronized(EthernetNetworkFactoryEx.this) {
                    if (mNetworkAgent != null) {
                        Log.e(TAG, "Already have a NetworkAgent - aborting new request");
                        stopIpManagerLocked();
                        mIpProvisioningThread = null;
                        return;
                    }
                    mLinkProperties = linkProperties;
                    mNetworkInfo.setIsAvailable(true);
                    mNetworkInfo.setDetailedState(DetailedState.CONNECTED, null, mHwAddr);

                    // Create our NetworkAgent.
                    mNetworkAgent = new NetworkAgent(mFactory.getLooper(), mContext,
                            NETWORK_TYPE, mNetworkInfo, mNetworkCapabilities, mLinkProperties,
                            NETWORK_SCORE) {
                        public void unwanted() {
                            synchronized(EthernetNetworkFactoryEx.this) {
                                if (this == mNetworkAgent) {
                                    stopIpManagerLocked();
                                    mDhcpResults.clear();
                                    mLinkProperties.clear();
                                    mNetworkInfo.setDetailedState(DetailedState.DISCONNECTED, null,
                                            mHwAddr);
				    if(mode.equals("dhcp")) {
                                       Log.w(TAG,"send dhcp disconnect broadcast");
				       sendEthernetStateBroadcast(EthernetManager.EVENT_DHCP_DISCONNECT_SUCCESSED);
				    } else if (mode.equals("manual")) {
                                       Log.w(TAG,"send static disconnect broadcast");
				       sendEthernetStateBroadcast(EthernetManager.EVENT_STATIC_DISCONNECT_SUCCESSED);
				    } else { //add for pppoe;
				       Log.w(TAG,"send pppoe disconnect broadcast");
                                       sendPppoeStateBroadcast(PppoeManager.EVENT_DISCONNECT_SUCCESSED);
                                       mPppoeState = PppoeManager.PPPOE_STATE_DISCONNECT;
				    }
                                    updateAgent();
                                    mNetworkAgent = null;
                                    try {
                                        mNMService.clearInterfaceAddresses(mIface);
                                    } catch (Exception e) {
                                        Log.e(TAG, "Failed to clear addresses or disable ipv6" + e);
                                    }
                                } else {
                                    Log.d(TAG, "Ignoring unwanted as we have a more modern " +
                                            "instance");
                                }
                            }
                        };
                        public void networkStatus(int status,String redirectUrl) {
                            Log.w(TAG,"enter networkStatus:"+status+":redirectUrl:"+redirectUrl);
                            if (status == NetworkAgent.VALID_NETWORK) {
                               if (mode.equals("dhcp")) {
                                 // sendEthernetStateBroadcast(EthernetManager.EVENT_DHCP_CONNECT_SUCCESSED);
                               }
                               else if (mode.equals("manual")) {
                                 // sendEthernetStateBroadcast(EthernetManager.EVENT_STATIC_CONNECT_SUCCESSED);
                               } else {
                                    ;//待添加对pppoe的处理。
                               }
                            }
                        }
                    };

                    mIpProvisioningThread = null;
                }

                if (DBG) {
                    Log.d(TAG, String.format("exiting ipProvisioningThread(%s): mNetworkInfo=%s",
                            mIface, mNetworkInfo));
                }
            }
        });
		synchronized(EthernetNetworkFactoryEx.this) {
            if (mIpProvisioningThread == null) {
                mIpProvisioningThread = ipProvisioningThread;
                mIpProvisioningThread.start();
            }
        }
        for(int i=0;i<100;++i) {
	     if(mConnectivityManager.getNetworkInfo(9).getState() == NetworkInfo.State.CONNECTED) {
                  if (mode.equals("dhcp")) {
                         sendEthernetStateBroadcast(EthernetManager.EVENT_DHCP_CONNECT_SUCCESSED);
                  }
                  else if (mode.equals("manual")) {
                         sendEthernetStateBroadcast(EthernetManager.EVENT_STATIC_CONNECT_SUCCESSED);
                  } else {
                         sendPppoeStateBroadcast(PppoeManager.EVENT_CONNECT_SUCCESSED);
                         mPppoeState = PppoeManager.PPPOE_STATE_CONNECT;
                  }
		  return ;
	      }
	      try {
			Thread.sleep(100);
	      } catch (Exception e) {
			Log.e(TAG,"Thread error");
	      }
	}
	      Log.e(TAG,"wait for ethernet become CONNECTED state timeout !!!");
    }

    /**
     * Begin monitoring connectivity
     */
    public synchronized void start(Context context, Handler target) {
        // The services we use.
        IBinder b = ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE);
        mNMService = INetworkManagementService.Stub.asInterface(b);

        // Interface match regex.
        mIfaceMatch = context.getResources().getString(
                com.android.internal.R.string.config_ethernet_iface_regex);

        // Create and register our NetworkFactory.
        mFactory = new LocalNetworkFactory(NETWORK_TYPE, context, target.getLooper());
        mFactory.setCapabilityFilter(mNetworkCapabilities);
        mFactory.setScoreFilter(-1); // this set high when we have an iface
        mFactory.register();

        mContext = context;

        // Start tracking interface change events.
        mInterfaceObserver = new InterfaceObserver();
        try {
            mNMService.registerObserver(mInterfaceObserver);
        } catch (RemoteException e) {
            Log.e(TAG, "Could not register InterfaceObserver " + e);
        }

        // If an Ethernet interface is already connected, start tracking that.
        // Otherwise, the first Ethernet interface to appear will be tracked.
        try {
            final String[] ifaces = mNMService.listInterfaces();
            for (String iface : ifaces) {
                synchronized(this) {
                    if (maybeTrackInterface(iface)) {
                        // We have our interface. Track it.
                        // Note: if the interface already has link (e.g., if we
                        // crashed and got restarted while it was running),
                        // we need to fake a link up notification so we start
                        // configuring it. Since we're already holding the lock,
                        // any real link up/down notification will only arrive
                        // after we've done this.
                        if (mNMService.getInterfaceConfig(iface).hasFlag("running")) {
                            updateInterfaceState(iface, true);
                        }
                        break;
                    }
                }
            }
        } catch (RemoteException|IllegalStateException e) {
            Log.e(TAG, "Could not get list of interfaces " + e);
        }
    }

    public synchronized void stop() {
        stopIpProvisioningThreadLocked();
        // ConnectivityService will only forget our NetworkAgent if we send it a NetworkInfo object
        // with a state of DISCONNECTED or SUSPENDED. So we can't simply clear our NetworkInfo here:
        // that sets the state to IDLE, and ConnectivityService will still think we're connected.
        //
        // TODO: stop using explicit comparisons to DISCONNECTED / SUSPENDED in ConnectivityService,
        // and instead use isConnectedOrConnecting().
        if (mEthernetManager == null) {
             mEthernetManager = (EthernetManager)mContext.getSystemService(Context.ETHERNET_SERVICE);
        }
        mNetworkInfo.setDetailedState(DetailedState.DISCONNECTED, null, mHwAddr);
        mLinkUp = false;
        updateAgent();
        mLinkProperties = new LinkProperties();
        mNetworkAgent = null;
        setInterfaceInfoLocked("", null);
        mNetworkInfo = new NetworkInfo(ConnectivityManager.TYPE_ETHERNET, 0, NETWORK_TYPE, "");
        mFactory.unregister();
        String mode = mEthernetManager.getEthernetMode();
        if (mode.equals("dhcp")) {
           sendEthernetStateBroadcast(EthernetManager.EVENT_DHCP_DISCONNECT_SUCCESSED);
        }
        else if (mode.equals("manual")) {
           sendEthernetStateBroadcast(EthernetManager.EVENT_STATIC_DISCONNECT_SUCCESSED);
        }
        else {
           sendPppoeStateBroadcast(PppoeManager.EVENT_DISCONNECT_SUCCESSED);
           stopPppoeNative("eth0");
           mPppoeState = PppoeManager.PPPOE_STATE_DISCONNECT;
        }
    }

    private void initNetworkCapabilities() {
        mNetworkCapabilities = new NetworkCapabilities();
        mNetworkCapabilities.addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET);
        mNetworkCapabilities.addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);
        mNetworkCapabilities.addCapability(NetworkCapabilities.NET_CAPABILITY_NOT_RESTRICTED);
        // We have no useful data on bandwidth. Say 100M up and 100M down. :-(
        mNetworkCapabilities.setLinkUpstreamBandwidthKbps(100 * 1000);
        mNetworkCapabilities.setLinkDownstreamBandwidthKbps(100 * 1000);
    }

    public synchronized boolean isTrackingInterface() {
        return !TextUtils.isEmpty(mIface);
    }
    public LinkProperties getPppoeLinkProperties(String iface) {
        LinkProperties linkProperties;

        synchronized(EthernetNetworkFactoryEx.this) {
            if (mNetworkAgent != null) {
                Log.e(TAG, "Already have a NetworkAgent - aborting new request");
                return null;
            }
            mIface = SystemProperties.get("net."+iface+"-pppoe.interface");
            linkProperties = new LinkProperties();
            linkProperties.setInterfaceName(this.mIface);

            String[] dnses = new String[2];
	        String route;
            String ipaddr;

	        dnses[0] = SystemProperties.get("net."+iface+"-ppp0.dns1");
            dnses[1] = SystemProperties.get("net."+iface+"-ppp0.dns2");

            ArrayList<InetAddress> dnsServers = new ArrayList<InetAddress>();
            dnsServers.add(NetworkUtils.numericToInetAddress(dnses[0]));
            dnsServers.add(NetworkUtils.numericToInetAddress(dnses[1]));
	        linkProperties.setDnsServers(dnsServers);

	        route = SystemProperties.get("net."+iface+"-ppp0.remote-ip");
            linkProperties.addRoute(new RouteInfo(NetworkUtils.numericToInetAddress(route)));

            ipaddr = SystemProperties.get("net."+iface+"-ppp0.local-ip");
            linkProperties.addLinkAddress(new LinkAddress(NetworkUtils.numericToInetAddress(ipaddr),28));


            //for getDhcpInfo use
            mDhcpResults.setIpAddress(ipaddr,28);
            mDhcpResults.setGateway(route);
            mDhcpResults.dnsServers = dnsServers;
            return linkProperties;
        }
	}
    /**
     * Set interface information and notify listeners if availability is changed.
     * This should be called with the lock held.
     */
    private void setInterfaceInfoLocked(String iface, String hwAddr) {
        boolean oldAvailable = isTrackingInterface();
        mIface = iface;
        mHwAddr = hwAddr;
        boolean available = isTrackingInterface();
    }

    synchronized void dump(FileDescriptor fd, IndentingPrintWriter pw, String[] args) {
        if (isTrackingInterface()) {
            pw.println("Tracking interface: " + mIface);
            pw.increaseIndent();
            pw.println("MAC address: " + mHwAddr);
            pw.println("Link state: " + (mLinkUp ? "up" : "down"));
            pw.decreaseIndent();
        } else {
            pw.println("Not tracking any interface");
        }

        pw.println();
        pw.println("NetworkInfo: " + mNetworkInfo);
        pw.println("LinkProperties: " + mLinkProperties);
        pw.println("NetworkAgent: " + mNetworkAgent);
        if (mIpManager != null) {
            pw.println("IpManager:");
            pw.increaseIndent();
            mIpManager.dump(fd, pw, args);
            pw.decreaseIndent();
        }
    }

    private void sendEthernetStateBroadcast(int event) {
        Intent intent = new Intent(EthernetManager.ETHERNET_STATE_CHANGED_ACTION);
        intent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT
                | Intent.FLAG_RECEIVER_REPLACE_PENDING);
        intent.putExtra(EthernetManager.EXTRA_ETHERNET_STATE, event);
        mContext.sendStickyBroadcastAsUser(intent, UserHandle.ALL);
    }

    private void sendPppoeStateBroadcast(int event) {
        Intent intent = new Intent(PppoeManager.PPPOE_STATE_CHANGED_ACTION);
        intent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT
                | Intent.FLAG_RECEIVER_REPLACE_PENDING);
        intent.putExtra(PppoeManager.EXTRA_PPPOE_STATE, event);
        mContext.sendStickyBroadcastAsUser(intent, UserHandle.ALL);
    }

    public boolean getNetLinkStatus() {
        return mLinkUp ? true : false;
    }

    public DhcpInfo getDhcpInfo() {

        DhcpResults dhcpResults = mDhcpResults;
        DhcpInfo info = new DhcpInfo();

        if (dhcpResults.ipAddress != null &&
                dhcpResults.ipAddress.getAddress() instanceof Inet4Address) {
            info.ipAddress = NetworkUtils.inetAddressToInt((Inet4Address) dhcpResults.ipAddress.getAddress());
            info.netmask = NetworkUtils.prefixLengthToNetmaskInt(dhcpResults.ipAddress.getPrefixLength());
        }

        if (dhcpResults.gateway != null) {
            info.gateway = NetworkUtils.inetAddressToInt((Inet4Address) dhcpResults.gateway);
        }

        int dnsFound = 0;
        for (InetAddress dns : dhcpResults.dnsServers) {
            if (dns instanceof Inet4Address) {
                if (dnsFound == 0) {
                    info.dns1 = NetworkUtils.inetAddressToInt((Inet4Address)dns);
                } else {
                    info.dns2 = NetworkUtils.inetAddressToInt((Inet4Address)dns);
                }
                if (++dnsFound > 1) break;
            }
        }
        Inet4Address serverAddress = dhcpResults.serverAddress;
        if (serverAddress != null) {
            info.serverAddress = NetworkUtils.inetAddressToInt(serverAddress);
        }
        info.leaseDuration = dhcpResults.leaseDuration;

        return info;
    }

    public int getPppoeState() {
        return mPppoeState;
    }
    public native static int startPppoeNative(String iface);
    public native static int stopPppoeNative(String iface);
    public native static int registerNatives();

}
