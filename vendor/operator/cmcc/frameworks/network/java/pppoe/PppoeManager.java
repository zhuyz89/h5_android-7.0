/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net.pppoe;


import android.content.Context;
import android.net.DhcpInfo;
import android.net.pppoe.PppoeManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.content.Context;
import android.util.Log;
import android.net.pppoe.IPppoeManager;
import android.net.IpConfiguration;

public class PppoeManager {
    public static final String TAG = "PppoeManager";

    public static final String PPPOE_STATE_CHANGED_ACTION = "PPPOE_STATE_CHANGED";
    public static final String EXTRA_PPPOE_STATE  = "pppoe_state";
    public static final String EXTRA_PPPOE_ERRMSG = "pppoe_errmsg";

    public static final int PPPOE_STATE_UNKNOWN    = 0;
    public static final int PPPOE_STATE_CONNECT    = 1;
    public static final int PPPOE_STATE_DISCONNECT = 2;
    public static final int PPPOE_STATE_CONNECTING = 3;

    public static final int EVENT_CONNECT_SUCCESSED        = 0;
    public static final int EVENT_CONNECT_FAILED           = 1;
    public static final int EVENT_CONNECT_FAILED_AUTH_FAIL = 2;
    public static final int EVENT_CONNECTING               = 3;
    public static final int EVENT_DISCONNECT_SUCCESSED     = 4;
    public static final int EVENT_DISCONNECT_FAILED        = 5;
    public static final int EVENT_AUTORECONNECTING         = 6;

    public static final int PPPOE_DISABLED            = 0;
    public static final int PPPOE_ENABLED             = 1;

    public static final int PPPOE_CONNECT_RESULT_UNKNOWN           = 10;
    public static final int PPPOE_CONNECT_RESULT_CONNECT           = 11;
    public static final int PPPOE_CONNECT_RESULT_CONNECTING        = 12;
    public static final int PPPOE_CONNECT_RESULT_DISCONNECTING     = 13;
    public static final int PPPOE_CONNECT_RESULT_DISCONNECT        = 14;
    public static final int PPPOE_CONNECT_RESULT_SERVER_DISCONNECT = 15;
    public static final int PPPOE_CONNECT_RESULT_AUTH_FAIL         = 16;
    public static final int PPPOE_CONNECT_RESULT_CONNECT_FAIL      = 17;

    public static final String PPPOE_CONNECT_MODE_DHCP = "dhcp";
    public static final String PPPOE_CONNECT_MODE_MANUAL = "manual";

    IPppoeManager mService;
    Handler mHandler;
    Context mContext;
    public PppoeManager(Context context,IPppoeManager service) {
        Log.d(TAG, "init PppoeManager");
        mService = service;
        mContext = context;
    }

    /**
    * Connect Pppoe, Dhcp by Default
    * @param name username
    * @param pswd password
    * @param ifaceName interface name, like eth0
    */
    public synchronized void connect(String name, String pswd, String ifaceName) {
        try {
            Log.d(TAG, "connect(" + name + ", " + pswd +", " + ifaceName + ")");
            mService.connect(name, pswd, ifaceName);
        } catch (RemoteException e) {
            Log.e(TAG, "connect(" + name + ", " + pswd +", " + ifaceName + ") ERROR!!");
        }
    }

    /**
    * Disconnect Pppoe
    * @param ifaceName interface name, like eth0
    */
    public synchronized void disconnect(String ifaceName) {
        try {
            Log.d(TAG, "disconnect(" + ifaceName + ")");
            mService.disconnect(ifaceName);
        } catch (RemoteException e) {
            Log.e(TAG, "disconnect(" + ifaceName + ") ERROR!!");
        }
    }

    /**
    * Set Pppoe Connection Mode
    * @param mode PPPOE_CONNECT_MODE_DHCP or PPPOE_CONNECT_MODE_MANUAL
    * @param info must not be null if mode is PPPOE_CONNECT_MODE_MANUAL
    */
    public synchronized void setPppoeMode(String mode, DhcpInfo info) {
        try {
            Log.d(TAG, "setPppoeMode(" + mode + ", " + info + ")");
            mService.setPppoeMode(mode, info);
        } catch (RemoteException e) {
            Log.e(TAG, "setPppoeMode(" + mode + ", " + info + ") ERROR!");
        }
    }

    /**
    * Get Pppoe Connection Mode
    * @return mode PPPOE_CONNECT_MODE_DHCP or PPPOE_CONNECT_MODE_MANUAL
    */
   /* public synchronized String getPppoeMode() {
        try {
            Log.d(TAG, "getPppoeMode() :" + mService.getPppoeMode());
            return mService.getPppoeMode();
        } catch (RemoteException e) {
            Log.e(TAG, "getPppoeMode() ERROR!");
            return null;
        }
    }*/

    /**
    * Get Pppoe Connection State
    * @return
    *  PPPOE_STATE_UNKNOWN
    *  PPPOE_STATE_CONNECT
    *  PPPOE_STATE_DISCONNECT
    *  PPPOE_STATE_CONNECTING
    */
    public int getPppoeState() {
        try {
            Log.d(TAG, "getPppoeState(): " + mService.getPppoeState());
            return mService.getPppoeState();
        } catch (RemoteException e) {
            Log.e(TAG, "getPppoeState() PPPOE_STATE_UNKNOWN!");
            return PPPOE_STATE_UNKNOWN;
        }
    }

    /**
    * Get Pppoe Enable/Disable State In Database
    * @return true for Enable, false for Disable
    */
    public boolean isPppoeDeviceUp() {
        try {
              Log.d(TAG, "isPppoeDeviceUp(): " + mService.isPppoeDeviceUp());
              return mService.isPppoeDeviceUp();
          } catch (RemoteException e) {
              Log.e(TAG, "isPppoeDeviceUp() ERROR!");
              return false;
          }
    }

    /**
    * Get Pppoe DhcpInfo
    * @return pppoe ip info
    */
    public DhcpInfo getDhcpInfo() {
        try {
            Log.d(TAG, "getDhcpInfo():" + mService.getDhcpInfo());
            return mService.getDhcpInfo();
        } catch (RemoteException e) {
            Log.e(TAG, "getDhcpInfo()! ERROR!");
            return null;
        }
    }

}

