/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.IEthernetServiceListener;
import android.net.IpConfiguration;
import android.net.StaticIpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;
import android.util.PrintWriterPrinter;
import android.net.DhcpInfo;
import android.net.DhcpResults;
import android.net.LinkProperties;
import android.net.NetworkInfo;
import android.net.LinkAddress;
import android.net.NetworkUtils;
import android.net.ethernet.EthernetManager;
import android.net.pppoe.PppoeManager;
import android.net.pppoe.IPppoeManager;
import android.content.ContentResolver;
import android.provider.Settings;
import android.content.Intent;
import android.os.UserHandle;
import android.os.SystemProperties;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicBoolean;
import java.net.InetAddress;
import java.net.Inet4Address;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;

import java.lang.Class;
import java.lang.reflect.Constructor;
import java.lang.Exception;

/**
 * EthernetServiceImpl handles remote Ethernet operation requests by implementing
 * the IEthernetManager interface.
 *
 * @hide
 */
public class PppoeService extends IPppoeManager.Stub {
    private static final String TAG = "PppoeService";
    private final Context mContext;
    private Handler mHandler;
    private EthernetManager mEthernetManager;
    private int mPppoeState = 0;

    private final String PPPOE_PAP_CONFIG_FILE      = "/data/misc/pppoe/pap-secrets";
    private final String PPPOE_CHAP_CONFIG_FILE     = "/data/misc/pppoe/chap-secrets";
    private final String PPPOE_INTERFACE            = "/data/misc/pppoe/pppoe-interface";
    private final String PPPOE_CONFIG_FORMAT        = "\"%s\" * \"%s\"";

    public PppoeService(Context context) {
        Log.d(TAG, "PppoeService Constructor!");
		mContext = context;
    }

    public DhcpInfo getDhcpInfo() {
	    return mEthernetManager.getDhcpInfo();
    }

    public synchronized void connect(String name,String pswd,String ifaceName) {
        if (mEthernetManager == null) {
            mEthernetManager = (EthernetManager) mContext.getSystemService(Context.ETHERNET_SERVICE);
        }
        if (mEthernetManager == null) {
           Log.d(TAG,"mEthernetManager = null");
        }
        setupPppoe(ifaceName,name,pswd);
        mEthernetManager.setEthernetMode(EthernetManager.ETHERNET_CONNECT_MODE_PPPOE,null);
        mEthernetManager.setEthernetEnabled(true);
        mPppoeState = PppoeManager.PPPOE_STATE_CONNECTING;
    }

    public synchronized void disconnect(String ifaceName) {
       if (mEthernetManager == null) {
          mEthernetManager = (EthernetManager) mContext.getSystemService(Context.ETHERNET_SERVICE);
       }
       if (mEthernetManager == null) {
          Log.d(TAG,"mEthernetManager = null");
       }
        mPppoeState = PppoeManager.PPPOE_STATE_DISCONNECT;
        mEthernetManager.setEthernetEnabled(false);
    }

    public int getPppoeState() {
        if (mEthernetManager == null) {
           mEthernetManager = (EthernetManager) mContext.getSystemService(Context.ETHERNET_SERVICE);
        }
              return  mEthernetManager.getPppoeState();
    }

    public boolean isPppoeDeviceUp() {
        if (mEthernetManager == null) {
            mEthernetManager = (EthernetManager) mContext.getSystemService(Context.ETHERNET_SERVICE);
        }
        return ((mEthernetManager.getEthernetState() == EthernetManager.ETHERNET_STATE_ENABLED) && (mEthernetManager.getEthernetMode() == EthernetManager.ETHERNET_CONNECT_MODE_PPPOE));
    }
    public void setPppoeMode(String mode,DhcpInfo dhcpinfo) {
        return ;
    }
    public boolean setupPppoe(String iface ,String user,String password) {
        String pap_file_path;
        String chap_file_path;
        String interface_file_path;
        pap_file_path = PPPOE_PAP_CONFIG_FILE;
        chap_file_path = PPPOE_CHAP_CONFIG_FILE;
        interface_file_path  = PPPOE_INTERFACE;
        File directory = new File("/data/misc/pppoe");
        if (!directory.isDirectory()) {
             Log.w(TAG,"create folder");
             directory.mkdir();
        }
        File pap_file = new File(pap_file_path);
        File chap_file = new File(chap_file_path);
        File interface_file  = new File(interface_file_path);
        String loginInfo = String.format(PPPOE_CONFIG_FORMAT, user, password);
         try {
            // 1. Save pap-secrets
            BufferedOutputStream out = new BufferedOutputStream(
                    new FileOutputStream(pap_file));
            // out.write(loginInfo.getBytes(), 0, loginInfo.length());
            out.write(user.getBytes());
            out.write("\n".getBytes());
            out.write(password.getBytes());
            Log.d(TAG, "write to " + PPPOE_PAP_CONFIG_FILE
                    + " login info = " + loginInfo);
            out.flush();
            out.close();
            // 2. Save chap-secrets
            out = new BufferedOutputStream(
                    new FileOutputStream(chap_file));
            out.write(loginInfo.getBytes(), 0, loginInfo.length());
            Log.d(TAG, "write to " + PPPOE_CHAP_CONFIG_FILE
                    + " login info = " + loginInfo);
            out.flush();
            out.close();

            out = new BufferedOutputStream(
                    new FileOutputStream(interface_file));
            out.write(iface.getBytes());
            Log.d(TAG, "write to " + PPPOE_INTERFACE
                    + " interface info = " + iface);
            out.flush();
            out.close();

            return true;
         } catch (IOException e) {
            Log.e(TAG, "Write PPPoE config failed!" + e);
            return false;
         }
    }

}
