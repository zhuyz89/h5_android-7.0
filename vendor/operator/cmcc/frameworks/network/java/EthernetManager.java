/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net.ethernet;

import android.annotation.SdkConstant;
import android.annotation.SdkConstant.SdkConstantType;

import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.content.Context;
import android.util.Log;
import android.net.ethernet.IEthernetManager;
import android.net.DhcpInfo;
import android.net.IpConfiguration;

/**
 * This class provides the primary API for managing all aspects of Ethernet
 * connectivity. Get an instance of this class by calling
 * {@link android.content.Context#getSystemService(String) Context.getSystemService(Context.ETHERNET_SERVICE)}.
 *
 * This is the API to use when performing Ethernet specific operations. To
 * perform operations that pertain to network connectivity at an abstract
 * level, use {@link android.net.ConnectivityManager}.
 */
public class EthernetManager {
    public static final String TAG = "EthernetManager";

    public static final String ETHERNET_STATE_CHANGED_ACTION =
            "android.net.ethernet.ETHERNET_STATE_CHANGE";
    public static final String EXTRA_ETHERNET_STATE = "ethernet_state";
    public static final int ETHERNET_STATE_DISABLED = 0;
    public static final int ETHERNET_STATE_ENABLED = 1;
    public static final int ETHERNET_STATE_UNKNOWN = 2;

    public static final int EVENT_DHCP_CONNECT_SUCCESSED = 10;
    public static final int EVENT_DHCP_CONNECT_FAILED = 11;
    public static final int EVENT_DHCP_DISCONNECT_SUCCESSED = 12;
    public static final int EVENT_DHCP_DISCONNECT_FAILED = 13;

    public static final int EVENT_STATIC_CONNECT_SUCCESSED = 14;
    public static final int EVENT_STATIC_CONNECT_FAILED = 15;
    public static final int EVENT_STATIC_DISCONNECT_SUCCESSED = 16;
    public static final int EVENT_STATIC_DISCONNECT_FAILED = 17;

    public static final int EVENT_PHY_LINK_UP =18;
    public static final int EVENT_PHY_LINK_DOWN = 19;

    /**
     * The ethernet interface is configured by dhcp
     */
    public static final String ETHERNET_CONNECT_MODE_DHCP = "dhcp";

    /**
     * The ethernet interface is configured manually
     */
    public static final String ETHERNET_CONNECT_MODE_MANUAL = "manual";

    /**
     * The ethernet interface is configured by pppoe
     */
    public static final String ETHERNET_CONNECT_MODE_PPPOE = "pppoe";

    IEthernetManager mService;
    private final Context mContext;

    public EthernetManager(Context context, IEthernetManager service) {
		Log.d(TAG, "Init EthernetManager");
        mContext = context;
        mService = service;
    }

    /**
     * Get If Ethernet Obtain Ip Mode Is Configured
     *
     * @return true for configured, false for not configured
     */
    public boolean isEthernetConfigured() {
        return true;
    }

    /**
     * Get Current Network DhcpInfo
     *
     * @return current network DhcpInfo
     */
    public DhcpInfo getDhcpInfo() {
        try {
            Log.d(TAG, "getDhcpInfo():" + mService.getDhcpInfo());
            return mService.getDhcpInfo();
        } catch (RemoteException e) {
            Log.e(TAG, "getDhcpInfo() ERROR!");
            return null;
        }
    }

    /**
     * Save Ethernet Obtain IP Mode And IP Info For Static IP.
     *
     * @param mode
     * ETHERNET_CONNECT_MODE_DHCP
     * ETHERNET_CONNECT_MODE_MANUAL
     * ETHERNET_CONNECT_MODE_PPPOE
     * @param dhcpInfo
     * if mode is ETHERNET_CONNECT_MODE_MANUAL，it is Required.
     */
    public void setEthernetMode(String mode, DhcpInfo dhcpInfo) {
        try {
            Log.d(TAG, "setEthernetMode(" + mode + ", " + dhcpInfo + ")");
            mService.setEthernetMode(mode, dhcpInfo);
        } catch (RemoteException e) {
            Log.e(TAG, "setEthernetMode(" + mode + ", " + dhcpInfo + ") ERROR!");
        }
    }

    /**
     * Get Etherent Obtain IP mode
     *
     * @return
     *  ETHERNET_CONNECT_MODE_DHCP
     *  ETHERNET_CONNECT_MODE_MANUAL
     *  ETHERNET_CONNECT_MODE_PPPOE
     *  null for error
     */
    public String getEthernetMode() {
        try {
            Log.d(TAG, "getEthernetMode(): " + mService.getEthernetMode());
            return mService.getEthernetMode();
        } catch (RemoteException e) {
            Log.e(TAG, "getEthernetMode() ERROR!");
            return null;
        }
    }

    /**
     * Get Ethernet Interface Name
     *
     * @return Ethernet Interface Name
     */
    public String getInterfaceName() {
        return "eth0";
    }

    /**
     * Set Ethernet Interface Name
     *
     * @param iface eth0, eth1, etc.
     * @return ture for Success, false for Fail
     */
    public boolean setInterfaceName(String iface) {
        /* do nothing */
		return true;
    }

    /**
     * Enable/Disable Ethernet
     * <p>
     *   Disable
     *     dhcp: stop dhcp, clear interface ip
     *     static: clear interface ip
     *   Enable:
     *     start dhcp/static ip and configure interface
     * </p>
     * @param enable ETHERNET_STATE_DISABLED or ETHERNET_STATE_ENABLED
     */
    public void setEthernetEnabled(boolean enable) {
        try {
            Log.d(TAG, "setEthernetEnabled(" + enable + ")");
            mService.setEthernetState(enable ? ETHERNET_STATE_ENABLED:ETHERNET_STATE_DISABLED);
        } catch (RemoteException e) {
            Log.e(TAG, "setEthernetEnabled(" + enable + ") ERROR!");
        }
    }

    /**
     * Get Real Time Ethernet State
     * @return
     *  ETHERNET_STATE_DISABLED
     *  ETHERNET_STATE_ENABLED
     *  ETHERNET_STATE_UNKNOWN
     */
    public int getEthernetState() {
        try {
            Log.d(TAG, "getEthernetState(): " + mService.getEthernetState());
            return mService.getEthernetState();
        } catch (RemoteException e) {
            Log.e(TAG, "getEthernetState() ETHERNET_STATE_UNKNOWN!");
            return ETHERNET_STATE_UNKNOWN;
        }
    }

    /**
     *  Set Ethernet Disguise Wifi Enable/Disable
     * <p>
     *    If some apk only work through Wifi, please Enable this.
     * </p>
     * @param enable
     */
    public void setWifiDisguise(boolean enable) {
        /* do nothing */
    }

    /**
     * Get Ethernet Disguise Wifi State
     * @return
     *  WIFI_DISGUISE_ENABLED         1
     *  WIFI_DISGUISE_DISABLED        0
     *  WIFI_DISGUISE_STATE_UNKNOWN   2
     */
    public int getWifiDisguiseState() {
        return 0;
    }

    /**
     * Get Default Interface Physical Connection State
     * @return true or false
     */
    public boolean getNetLinkStatus() {
        Log.d(TAG, "getNetLinkStatus()" + (getNetLinkStatus(getInterfaceName()) > 0) );
        return getNetLinkStatus(getInterfaceName()) > 0;
    }

    /**
     * Get Physical Connection State
     * @param ifaceName
     * @return 1 for LinkUp, 0 for LinkDown, -1 for No Interface
     */
    public int getNetLinkStatus(String ifaceName) {
        try {
            Log.d(TAG, "getNetLinkStatus(" + ifaceName + "): " + mService.getNetLinkStatus(ifaceName));
            return mService.getNetLinkStatus(ifaceName);
        } catch (RemoteException e) {
            Log.e(TAG, "getNetLinkStatus ERROR!");
            return -1; //-1 represent unkown.
        }
    }

    public IpConfiguration getConfiguration() {
        try {
           Log.d(TAG,"getConfiguration");
           return mService.getConfiguration();
        } catch (NullPointerException | RemoteException e) {
           Log.e(TAG, "getConfiguration failed!"+e);
           return new IpConfiguration();
       }
    }
    public int getPppoeState() {
       try {
          return mService.getPppoeState();
       } catch (RemoteException e) {
          Log.e(TAG,"get Pppoe State failed");
          return -1;
       }
    }
}
